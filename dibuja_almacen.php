<table>
	<thead>
	    <tr>
	  		<?php for($n=0; $n<count($this->pilas); $n++) { ?>
	      		<th>Pila <?php echo $n+1; ?></th>
	    	<?php } ?>
	    </tr>
	</thead>
	<tbody>
		<?php for($i=3;$i>=0;$i--) { ?>
			<tr>
				<?php foreach($this->pilas as $pila) { ?>
					<?php if(isset($pila->lbox[$i])) { ?>
				  		<td><?php $pila->lbox[$i]->escribe(); ?></td>
					<?php } else { ?>
				  		<td><?php echo '[]' ?></td>
					<?php } ?>
				<?php } ?>
			</tr>
		<?php } ?>
	</tbody>
</table>