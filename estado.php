<?php
	class estado {
		var $listaProd;
		var $almacen;
		var $padre;
		var $g,$h,$f;

		function __construct($listaProd,$almacen,$padre) {
			$this->listaProd = $listaProd;
			$this->almacen = $almacen;
			$this->padre = $padre;
			$this->f(0);
		}

		function escribe() {
			echo '<br>Lista de producción:<br>';
			if(empty($this->listaProd)) {
				echo '[]';
			} else {
				$i = 0;
				foreach($this->listaProd as $box) {
					$separador = $i == 0 ? '' : ' | ';
					echo $separador;
					$box->escribe();
					$i++;
				}
			}

			echo '<br>Almacén:<br>';
			$this->almacen->dibuja();
		}

		/******************************************************************************************************************************/
		/* AQUÍ ES DONDE SE CALCULA LA FUNCIÓN DE EVALUACIÓN EN FUNCIÓN DE LA HEURÍSTICA **********************************************/
		/******************************************************************************************************************************/
		function f($sumaFechasTotal) {
			$this->g();$this->h($sumaFechasTotal);
			$this->f = $this->g + $this->h;	// F = G + H
		}

		function g() {
			$this->g = ($this->padre == NULL) ? 0 : $this->padre->g + 1;	// G = Nº de cajas apiladas = Profundidad en el árbol = Nº de movimientos
		}

		// H = Puntuación que favorece a los estados que han introducido primero las cajas con la fecha de salida más lejana para que queden debajo, y además en la pila más lejana
		function h($sumaFechasTotal) {
			$this->h = 0;
			if($this->padre != NULL) {
				$sumaFechasApiladas = 0;
				$huecosLibresPonderados = 0;
				foreach($this->almacen->pilas as $i => $pila) {
					foreach($pila->lbox as $j => $box) {
						$sumaFechasApiladas += $box->diasalida;	// Sumamos las fechas de salida de las cajas que ya hay puestas
					}
					$huecosLibresPonderados += ($pila->limite - $pila->actual) * $i;	// Nº de huecos desocupados * Nº de pila: Asignamos un peso a los huecos desocupados en función de la pila en la que se encuentren, con lo que será más bajo cuantos menos huecos libres haya en las últimas pilas 
				}
				// Relaciona poner las cajas más tardías al final con que no queden huesos libres al fondo
				$this->h = ($sumaFechasTotal - $sumaFechasApiladas) * $huecosLibresPonderados;	// (Sum(fechas de cajas totales) - sum(fechas de cajas apiladas)) x Nº de huecos desocupados ponderados en función de la pila en la que se encuentren
			}
		}
		/******************************************************************************************************************************/
		/******************************************************************************************************************************/
		/******************************************************************************************************************************/

		function ascendientes() {
			$ascendientes = [];
			$padre = $this->padre;
			while($padre != NULL) {
				$ascendientes[] = $padre;
				$padre = $padre->padre;
			}
			return $ascendientes;
		}
	}