<?php
	class box {
		var $identificador;
		var $diaentrada;
		var $diasalida;

		function __construct($id,$entrada,$salida) {
			$this->identificador = $id;
			$this->diaentrada = $entrada;
			$this->diasalida = $salida;
		}

		function escribe() {
			echo 'b(' . $this->identificador . ',' . $this->diaentrada . ',' . $this->diasalida . ')';
		}
	}