<?php
	class almacen {
		var $pilas;

		function crea($pilas) {
			$this->pilas = $pilas;
		}

		function escribe() {
			foreach($this->pilas as $i => $pila) {
				$pila->escribe();
			}
		}

		function dibuja() {
			ob_start();
			include("dibuja_almacen.php");
			$output = ob_get_contents();
			ob_end_clean();
			echo $output;
		}
	}
