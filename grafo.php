<?php
	class grafo {

		var $estadoInicial;
		var $abiertos;
		var $cerrados;
		var $sumaFechasTotal;

		function __construct() {
			$listaProdInicial = [(new box(9,1,17)),(new box(10,1,17)),(new box(11,1,17)),(new box(12,1,17)),(new box(13,1,17)),(new box(14,1,17)),(new box(15,1,17)),(new box(16,1,17)),(new box(17,1,17)),(new box(18,1,17)),(new box(19,1,17)),(new box(20,1,17)),(new box(3,1,18)),(new box(4,1,18)),(new box(5,1,18)),(new box(6,1,18)),(new box(7,1,18)),(new box(8,1,18)),(new box(1,1,19)),(new box(2,1,19))];
			$almacenInicial = new almacen();
			$almacenInicial->crea( [ (new pila([],0,4)),(new pila([],0,4)),(new pila([],0,4)),(new pila([],0,4)),(new pila([],0,4)) ] );

			$this->sumaFechasTotal = 0;
			foreach($listaProdInicial as $box) {
				$this->sumaFechasTotal += $box->diasalida;
			}

			/* INSTANCIAMOS EL ESTADO INICIAL ****************************************/
			$this->estadoInicial = new estado($listaProdInicial,$almacenInicial,NULL);
			/*************************************************************************/

			$this->abiertos = [$this->estadoInicial];	// 1 - Iniciar el Grafo de exploración con s una lista llamada ABIERTOS
			$this->cerrados = [];	// 2 - Crear una lista llamada CERRADOS que inicialmente estará vacía.
		}


		/*************************************************************************************************************************/
		/* AQUÍ ES DONDE OCURRE CASI TODA LA MAGIA *******************************************************************************/
		/*************************************************************************************************************************/
		function resuelve() {
			$success = 0;
			while(!empty($this->abiertos)) {
				$n = array_shift($this->abiertos);	// 4 - Seleccionar el primer nodo de ABIERTOS
				array_push($this->cerrados,$n);	// 4 - Incluir el nodo en CERRADOS
				if($this->es_solucion($n)) {$success = 1; break; }	//5 - Si n es un nodo objetivo, salir
				$m = $this->expande($n);	// 6 | Devuelve un conjunto de nodos sucesores
				$this->establece_apuntadores($m,$n);	// 7 - Establecer un apuntador a n desde aquellos miembros de m...
				$this->abiertos = array_merge($m,$this->abiertos);	// 7 - Añadir los miembros de M (que no están ya en ABIERTOS) a ABIERTOS
				$this->ordena_abiertos();	//8 - Reordenar la lista ABIERTOS con arreglo al mérito heurístico
			}
			$this->muestra_solucion($n,$success); // Mostrar el camino hasta el nodo solución a través de los apuntadores de n
		}
		/*************************************************************************************************************************/
		/*************************************************************************************************************************/
		/*************************************************************************************************************************/


		function expande($n) {
			$posibles_sucesores = [];
			foreach($n->listaProd as $j => $box) {	// Probamos con cada una de las cajas que hay en ListaProd
				foreach($n->almacen->pilas as $i => $pila) {	// Probamos a apilar cada una de las cajas de ListaProd en cada una de las pilas
					$new_estado = $this->deepClone($n);	// Copiamos el estado anterior para poder escribir sobre él (Seguir apilando cajas)
					if($new_estado->almacen->pilas[$i]->apila($box)) {	// Si la caja es apilable...
						unset($new_estado->listaProd[$j]);	// Eliminamos la caja utilizada de la Lista de Producción 
						if(!($this->presente_en($new_estado,$new_estado->ascendientes()))) {	// Si no es un estado antecesor...
							$posibles_sucesores[] = $new_estado;	// Añadimos el nuevo estado al array de posibles sucesores de n
						}
					}
				}
			}
			return $posibles_sucesores;
		}

		function establece_apuntadores(&$m,$n) {	// 7 - Establecer un apuntador a n desde aquellos miembros de m...
			foreach($m as $i => $sucesor) {
				$presente_en_abiertos = $this->presente_en($sucesor,$this->abiertos);
				$presente_en_cerrados = $this->presente_en($sucesor,$this->cerrados);
				if(!$presente_en_abiertos && !$presente_en_cerrados) {	// 7 - ... Para cada miembro de M que ya figurase en ABIERTOS o CERRADOS
					$sucesor->padre = $n;
					$sucesor->f($this->sumaFechasTotal);	// Asignar valor a la función de evaluación a partir de la heurística //
				} else {
					if(($presente_en_abiertos != FALSE) && ($sucesor->f < $presente_en_abiertos->f)) {	// Los miembros de M que ya estaban en ABIERTOS...
						$presente_en_abiertos->padre = $sucesor->padre;	// Cambian sus apuntadores al "mejor padre"
					}
					if(($presente_en_cerrados != FALSE) && ($sucesor->f < $presente_en_cerrados->f)) {	// Para los miembros de M que estaban en CERRADOS...
						foreach($this->abiertos as $abierto) {	// Recorremos sus sucesores en abiertos y cambiamos sus apuntadores
							if($abierto->padre->almacen == $presente_en_cerrados->almacen) {
								$abierto->padre = $sucesor;
							}
						}
					}
				}
			}
		}

		function muestra_solucion($n,$success) {
			echo '<br><b><u>Solución:</u></b><br>';
			if($success == 1) { 
				foreach((array_reverse($n->ascendientes())) as $ascendiente) {	// Introducimos todos los ascendientes de n en un array
					$ascendiente->escribe();	// Mostramos los ascendientes
				}
				$n->escribe();
			} else { echo 'FALLO'; }
		}

		function presente_en($estado,$array_de_estados) {	// Comprobamos si un estado (configuración del almacén en un momento determinado) se encuentra presente en un array de estados
			foreach($array_de_estados as $est) {
				if(($estado->almacen == $est->almacen)) {
					return $est;
				}
			}
			return FALSE;
		}

		function ordena_abiertos() {	//Algoritmo de ordenación ShellSort (Menor a mayor F)
			$x = round(count($this->abiertos)/2);
			while($x > 0) {
				for($i = $x; $i < count($this->abiertos);$i++) {
					$temp = $this->abiertos[$i];
					$j = $i;
					while($j >= $x && $this->abiertos[$j-$x]->f > $temp->f) {
						$this->abiertos[$j] = $this->abiertos[$j - $x];
						$j -= $x;
					}
					$this->abiertos[$j] = $temp;
				}
				$x = round($x/2.2);
			}
		}

		function es_solucion($n) {
			$pilas_llenas = true;
			foreach($n->almacen->pilas as $pila) {
				if($pila->actual < $pila->limite) {
					$pilas_llenas = false;
					break;
				}
			}
			return (empty($n->listaProd) || $pilas_llenas);	// Hemos llegado a un nodo solución si la lista de producción está vacía o si las pilas están llenas
		}

		function deepClone($object)
		{
		    return unserialize(serialize($object));
		}
	}