<?php
	class pila {
		var $lbox;
		var $actual;
		var $limite;

		function __construct($lbox,$actual,$limite) {
			$this->lbox = $lbox;
			$this->actual = $actual;
			$this->limite = $limite;
		}

		function escribe() {
			if(empty($this->lbox)) {
				echo '[]';
			} else {
				foreach($this->lbox as $i => $box) {
					$box->escribe();
				}
			}
		}

		function apila($box) {	// Ponemos una caja sobre otra si no sobrepasamos el límite de capacidad de la pila y la nueva caja sale antes o el mismo día que la anterior ya apilada
			if( (empty($this->lbox)) || (($this->actual < $this->limite)/* && ($box->diasalida <= end($this->lbox)->diasalida)*/) ) {	// Podemos prescindir de la restricción porque la heurística se va a encargar de que no haya cajas más añejas apiladas encima de cajas menos añejas
				$this->lbox[]=$box;
				$this->actual++;
				return true;
			}
			return false;
		}
	}