<?php
// g(n) = Nº de cajas apiladas = Profundidad en el árbol = Nº de movimientos
// h(n) = ($sumaFechasTotal - $sumaFechasApiladas) * $huecosLibresPonderados || (Suma de las fechas de salida de todas las cajas - Suma de las fechas de salida de las cajas apiladas) x Nº de huecos libres en función de sus pesos (Un hueco libre tendrá más peso cuanto más alto sea el valor de la pila donde esté). El resultado será un número positivo o igual a 0 que favorece a aquellos estados que han apilado primero las cajas con salida más lejana en las pilas del fondo.
// f(n) = g(n) + h(n)

// h(n) Es admisible porque la diferencia entre la suma de las fechas de salida de todas las cajas Y la suma de las fechas de salida de las cajas apiladas SIEMPRE va a ser menor o igual a la suma de fechas de salida de todas las cajas. Por otro lado el peso de los huecos libres siempre va a ser menor en un almacén parcialmente ocupado que en un almacén con todos los huecos libres.

	include('box.php');
	include('pila.php');
	include('almacen.php');
	include('estado.php');
	include('grafo.php');

	$grafo = new grafo();

	echo '<b><u>Estado inicial:</u></b><br>';
	$grafo->estadoInicial->escribe();
	echo '<br>';

	$grafo->resuelve();